package com.adobe.aem.guides.wknd.core.models;

public interface Calculadora {

	String getTitulo();

	String getAccion();

	String getValorUno();

	String getValorDos();
	
	String getResultado();

	/***
	 * @return a boolean if the component has content to display.
	 */
	boolean isEmpty();
}