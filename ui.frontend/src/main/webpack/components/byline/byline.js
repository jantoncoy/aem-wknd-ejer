import jQuery from "jquery";

jQuery( function($){
    "use strict";

    function mostrarServicio(){
        let dominio = window.location.origin;
        let servicio = "/bin/servlet/listarOperaciones";
        let ok =(data) => { console.log(data);};
        let error = (data) => {console.error("Error producido: " + data);};
        $.get(dominio+servicio,ok).fail(error);
    }

    mostrarServicio();
});
