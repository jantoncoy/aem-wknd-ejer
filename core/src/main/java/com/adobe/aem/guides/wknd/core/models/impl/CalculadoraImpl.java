package com.adobe.aem.guides.wknd.core.models.impl;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.apache.sling.models.factory.ModelFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.components.ComponentContext;
import com.adobe.aem.guides.wknd.core.models.Calculadora;

@Model(
        adaptables = {SlingHttpServletRequest.class},
        adapters = {Calculadora.class},
        resourceType = {CalculadoraImpl.RESOURCE_TYPE},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL
)
public class CalculadoraImpl implements Calculadora {

    protected static final String RESOURCE_TYPE = "wknd/components/calculadora";

    @Self
    private SlingHttpServletRequest request;

    @OSGiService
    private ModelFactory modelFactory;

    @ScriptVariable
    private Page currentPage;

    @ScriptVariable
    protected ComponentContext componentContext;

    @ValueMapValue
    private String titulo;

    @ValueMapValue
    private String accion;

    @ValueMapValue
    private String valoruno;

    @ValueMapValue
    private String valordos;

    @ValueMapValue
    private String resultado;

    // Add a logger for any errors
    private static final Logger LOGGER = LoggerFactory.getLogger(BylineImpl.class);

    @PostConstruct
    private void init() {
    }

    @Override
    public boolean isEmpty() {

        if (StringUtils.isBlank(titulo)) {
            return true;
        }else{
            return false;
        }

    }

    @Override
    public String getTitulo() {
       return titulo != null ? titulo : "Titulo desconocido";
    }

    @Override
    public String getAccion() {
        return accion != null ? accion : "Accion Desconocida";
    }

    @Override
    public String getValorUno() {
        return valoruno != null ? valoruno : "Valor Uno Desconocido";
    }

    @Override
    public String getValorDos() {
        return valordos != null ? valordos : "Valor Dos Desconocido";
    }

    @Override
    public String getResultado() {
        if(valoruno != null && valordos != null){
           int uno = Integer.parseInt(valoruno);
           int dos = Integer.parseInt(valordos);
           return ((uno+dos) +"");
        }else{
            return "Resultado no calculable";
        }
    }
}